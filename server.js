const express = require("express");
const axios = require('axios');
const cheerio = require('cheerio');
const app = express();

const bodyParser = require('body-parser');

app.set('port', (process.env.PORT || 5000));
//Express session


app.use(bodyParser.json()); // support json encoded bodies
app.use(function (req, res, next) {
    res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials", true);
    next();
});

app.get('/ping', (req, res)=>{
    res.json({message:"success"});
});
app.get('/stock', (req, res)=>{
    const url = 'https://www.sharenet.co.za/v3/indices.php';

    axios(url)
        .then(response => {
            const html = response.data;
            const $ = cheerio.load(html);
            const statsTable = $("table > tbody > tr");
            const indices = [];

            statsTable.each(function () {
                const name = $(this).find('.sp_dataCell_alt > span').text().trim();
                const rp = $(this).find('td:nth-child(2)').text().trim();
                const move = $(this).find('td:nth-child(3)').text().trim();
                const percentageMove = $(this).find('td:nth-child(4)').text().trim();

                indices.push({
                    name,
                    rp,
                    move,
                    percentageMove,
                });
            });
            res.json(indices);
        })
        .catch(error =>  res.json({message:error}));

});


app.listen(app.get('port'), function () {
    console.log('Node app is running on port', app.get('port'));
});
